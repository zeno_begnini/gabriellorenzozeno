import java.util.*;

public class amicizia {
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Componi il numero di telefono della persona");
        System.err.println("E' in casa?");
        if (sc.nextBoolean()) {
            chiamata();
        } else {
            System.out.println("Lascia un messaggio");
            System.out.println("Aspetta di essere richiamato");
            chiamata();
        }
        sc.close();
        
    }  

    public static boolean bevande() {
        Scanner s = new Scanner(System.in);
        Scanner altro = new Scanner(System.in);
        System.out.println("vuoi bere qualcosa?");
        Boolean r = s.nextBoolean();
        if(r){
            System.out.println("scegli: té, caffé, cioccolata");
            String bevanda = altro.nextLine();
            if(bevanda.equals("cioccolata") ){
                System.out.println("facciamoci sta " + bevanda);
            }
            else{
                System.out.println("facciamoci sto " + bevanda);
            } 
            return true;
        }
        else{
            svago();
            return false;
        }
        
    } 
    
    public static void chiamata() {
        Scanner sc = new Scanner(System.in);

        System.err.println("Ti va di mangiare qualcosa insieme?");
        System.out.println("Ascolta la risposta");
        if (sc.nextBoolean()) {
            System.err.println("Mangiate qualcosa insieme");
            System.out.println("Siete diventati amici!\nOra hai una persona in più a cui poter rompere le palle in caso di bisogno e viceversa");
        } else {
            if( bevande()){
                System.out.println("Siete diventati amici!\nOra hai una persona in più a cui poter rompere le palle in caso di bisogno e viceversa");
            }

        }
        
    }

  

    public static boolean svago() {
        int n = 0;
        while(n < 6){
            Scanner sca = new Scanner(System.in);
            System.out.println("Allora svaghiamoci un po: cosa ti va di fare? ");
            String sva = "";
            sva = sca.nextLine();
            System.out.println("fare "+ sva + " è una cosa che può piacere a me? ");
            Boolean ris;
            ris = sca.nextBoolean();
            if (ris){
                System.out.println("Facciamolo insieme");
                System.out.println("Siete diventati amici!\nOra hai una persona in più a cui poter rompere le palle in caso di bisogno e viceversa");
                return true;
                
            }
            else{
                n += 1;
            }
        }
        System.out.println("Scegli l'opzione meno disumana e svagatevi insieme");
        System.out.println("Siete diventati amici!\nOra hai una persona in più a cui poter rompere le palle in caso di bisogno e viceversa");
        return true;
    }



}
